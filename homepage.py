from app import create_app
import os

app = create_app(os.environ.get("FLASK_CONFIG", 'production'))


@app.cli.command()
def test():

    import unittest

    tests = unittest.TestLoader().discover('tests')

    unittest.TextTestRunner(verbosity=2).run(tests)
