FROM python:3.8-buster
RUN useradd --create-home flaskuser

WORKDIR /home/homepage

COPY . /home/homepage

RUN chown -R flaskuser /home/homepage
RUN chmod -R 750 /home/homepage

RUN python3 -m pip install -r requirements.txt

ENV FLASK_CONFIG production
ENV FLASK_APP homepage

USER flaskuser

EXPOSE 5000

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "--capture-output", "homepage:app"]