import magic
from flask_login import current_user
from flask import abort, current_app


def under_construction(f):

    def inner(*args, **kwargs):

        if current_app.config.get('PRODUCTION'):
            abort(501)

        return f(*args, **kwargs)

    inner.__name__ = f.__name__

    return inner


def check_picture(pic):

    if pic:

        mtype = magic.from_buffer(pic)
        supertype = mtype.split('/')[0]

        return supertype == 'image'

    return False


def permissions_required(*args):

    def inner_dec(f):

        def inner(*iargs, **kwargs):

            if current_user.has_permissions(args):
                print(current_user.permissions)
                return f(*iargs, **kwargs)
            else:
                abort(401)

        inner.__name__ = f.__name__
        return inner

    return inner_dec
