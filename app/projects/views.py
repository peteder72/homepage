from flask import abort, flash, g, redirect, render_template, url_for

from app import db
from app.forms import ProjectForm
from app.models import Project, ProjectState, Permission
from flask_login import login_required, current_user
from app.projects import projects
from app.security import permissions_required


@projects.route("/")
def projects_page():
    return render_template('projects.html', projects=Project.query.all())


@projects.route("/<int:pid>")
def project(pid):

    pr = Project.query.filter_by(id=pid).first()

    if not pr:
        abort(404)

    return render_template('project_ind.html', project=pr)


@projects.route("/new", methods=['GET', 'POST'])
@login_required
@permissions_required(Permission.CREATE_PROJECT)
def new_project():

    project_form = ProjectForm()

    if project_form.validate_on_submit():

        try:
            mode = ProjectState(project_form.status.data)
        except ValueError:
            abort(400)

        if project_form.pic_url:
            pic_url = project_form.pic_url
        elif project_form.pic_upload:
            raise NotImplementedError()

        project = Project(
            uid=current_user.id,
            picture=pic_url,
            title=project_form.name.data,
            body=project_form.body.data,
            start_date=project_form.start_date.data,
            status=mode.value,
            finish_date=project_form.finish_date.data if mode == ProjectState.Finished else None
        )

        db.session.add(project)
        db.session.commit()

        return redirect(url_for('.project', pid=project.id))

    else:
        for fieldName, errorMessages in project_form.errors.items():
            for err in errorMessages:
                flash("Error for {}: {}".format(fieldName, err), 'error')

        return render_template('project_new.html', form=project_form)


@projects.before_app_request
def injectEnum():

    g.project_states = {s.value: s.name for s in ProjectState}
