from app import db
import datetime
import libgravatar
import enum
from app import login_manager
from flask_login import UserMixin, AnonymousUserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class Permission():

    ADMIN = 1
    CREATE_PROJECT = 2
    WRITE_COMMENT = 4
    LIKE = 8


class ProjectState(enum.Enum):

    Planned = 0
    WIP = 1
    Frozen = 2
    Postponed = 3
    Canceled = 4
    Finished = 5


class Project(db.Model):

    __tablename__ = 'project'

    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.Integer, db.ForeignKey('user.id'))

    title = db.Column(db.String(64))
    picture = db.Column(db.String(256))
    body = db.Column(db.Text)
    status = db.Column(db.Integer)

    start_date = db.Column(db.Date)
    finish_date = db.Column(db.Date)


class User(UserMixin, db.Model):

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    email_plain = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    self_description = db.Column(db.String(1024))

    joined = db.Column(db.Date, default=datetime.date.today)

    confirmed = db.Column(db.Boolean, default=False)
    last_sent_conf = db.Column(db.Integer)

    name = db.Column(db.String(64))
    surname = db.Column(db.String(64))

    pwd_hash = db.Column(db.String(256))
    email_hash = db.Column(db.String(256))

    permissions = db.Column(
        db.Integer,
        default=Permission.LIKE + Permission.WRITE_COMMENT
    )

    project = db.relationship("Project")

    @property
    def email(self):
        return self.email_plain

    @email.setter
    def email(self, v):
        self.email_plain = libgravatar.sanitize_email(v)
        self.email_hash = libgravatar.md5_hash(self.email_plain)

    @property
    def password(self):
        raise AttributeError("I'm not cracking hashes")

    @password.setter
    def password(self, v):
        self.pwd_hash = generate_password_hash(v)

    def verify_password(self, passw):
        return check_password_hash(self.pwd_hash, passw)

    def is_logged_in(self):
        return True

    def has_permission(self, perm: Permission):
        if self.permissions is None:
            return False

        if self.admin():
            return True

        return bool(int(self.permissions) & perm)

    def admin(self):
        return bool(int(self.permissions) & Permission.ADMIN)

    def has_permissions(self, args):

        res = True

        for arg in args:
            if type(arg) == int:
                res = res and self.has_permission(arg)
            elif type(arg) == list:
                tres = False
                for perm in arg:
                    tres = tres or self.has_permission(perm)
                res = res and tres
            if not res:
                break

        return res


class AnonymousUser(AnonymousUserMixin):

    def has_permission(self, perm):
        return False

    def has_permissions(self, args):
        return False

    def admin(self):
        return False

    def is_logged_in(self):
        return False


login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(uid):
    return User.query.get(int(uid))
