from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_pagedown import PageDown
from flask_migrate import Migrate, upgrade
from flask_misaka import Misaka
from werkzeug.middleware.proxy_fix import ProxyFix

from flask_login import LoginManager
import datetime

import time

from configs import config

__version__ = '1.0'

db = SQLAlchemy()
login_manager = LoginManager()
migrate = Migrate()
pagedown = PageDown()
md = Misaka(escape=True, fenced_code=True, highlight=True, strikethrough=True, autolink=True)

from .fake import gen_projects, gen_users
from app.main import main
from app.projects import projects

# Init file

from .forms import LoginForm, SignUpForm


def create_app(conf='production', populate_sample=True):

    app = Flask(__name__)

    app.config.from_object(config[conf])
    app.config['version'] = __version__

    from app import models

    # Not the most elegant solution, will have to revise this
    @app.context_processor
    def inject_vars():
        return {
            'permission': models.Permission,
            'now': datetime.datetime.now()
        }

    db.init_app(app)
    migrate.init_app(app, db)
    md.init_app(app)
    login_manager.init_app(app)
    pagedown.init_app(app)
    Bootstrap(app)

    with app.app_context():

        if app.debug and populate_sample:

            db.create_all()

            gen_users(10)
            gen_projects(10)

            user = models.User(
                email='admin@example.com',
                username='admin',
                password='123',
                last_sent_conf=time.time(),
                name='Foo',
                surname='Bar',
                permissions=models.Permission.ADMIN
            )

            db.session.add(user)
            db.session.commit()

    if app.config.get('PRODUCTION'):

        with app.app_context():

            upgrade()

    app.wsgi_app = ProxyFix(app.wsgi_app, x_host=1)
    app.register_blueprint(main, url_prefix='/', static_url_path='/static')
    app.register_blueprint(projects, url_prefix='/projects', static_url_path='/static')

    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    return app
