import requests
from flask_pagedown.fields import PageDownField
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import (BooleanField, HiddenField, PasswordField, SelectField,
                     StringField, SubmitField, ValidationError)
from wtforms.fields.html5 import DateField, EmailField
from wtforms.validators import (URL, DataRequired, Email, EqualTo, Length,
                                Regexp, Optional)

import app.regex as regex
from app.security import check_picture

from .models import User, ProjectState


def checkFormPic(form, field):

    if type(field) == FileField:
        pic = field.data
    elif type(field) == StringField:
        r = requests.get(field.data)
        if r.status_code != 200:
            return False
        pic = r.content

    return check_picture(pic)


class LoginForm(FlaskForm):

    email = StringField("E-mail or login", validators=[
        DataRequired(),
        Length(1, 64)
    ])
    password = PasswordField("Password", validators=[
        DataRequired()
    ])
    remember_me = BooleanField("Keep me logged in")
    submit = SubmitField("Log in")

    form_type = HiddenField("login", default='login')


class SignUpForm(FlaskForm):

    username = StringField("Username", validators=[
        DataRequired(),
        Regexp(
            regex.username_reg,
            0,
            "Username must start with a number and have only letters, numbers, dots or underscores"
        )
    ])

    name = StringField("Name", validators=[DataRequired()])
    surname = StringField("Surname", validators=[DataRequired()])

    email = EmailField("E-mail", validators=[
        DataRequired(),
        Length(1, 64),
        Email()
    ])

    password = PasswordField("Password", validators=[
        DataRequired(), EqualTo('re_password', message='Passwords do not match')
    ])

    re_password = PasswordField("Repeat password", validators=[
        DataRequired()
    ])

    submit = SubmitField("Sign up")

    form_type = HiddenField("signup", default='signup')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError("Username already in use")

    def validate_email(self, field):
        if User.query.filter_by(email_plain=field.data).first():
            raise ValidationError("E-mail already in use")


class ProjectForm(FlaskForm):

    name = StringField("Project name", validators=[
        DataRequired(), Length(1, 64)
    ])

    start_date = DateField("Project start date", validators=[Optional()], format="%Y-%m-%d")
    finish_date = DateField("Project end date (if finished)", validators=[Optional()], format="%Y-%m-%d")

    status = SelectField("Project status", choices=[(s.value, s.name) for s in ProjectState], coerce=int)

    body = PageDownField("The making of the project", validators=[DataRequired()])

    pic_upload = FileField('Upload the picture', validators=[Optional(), checkFormPic])
    pic_url = StringField("Picture URL", validators=[Optional(), URL(), checkFormPic])

    submit = SubmitField("Add a project")
